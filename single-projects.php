<?php get_header(); ?>
<?php
the_post();
$prefix                = '_kt_page_';
$projects_desc         = get_post_meta( get_the_ID(), $prefix . 'projects_desc', true );
$projects_status       = get_post_meta( get_the_ID(), $prefix . 'projects_status', true ) == '0' ? 'Hoàn Thành' : 'Đang lắp đặt' ;
$projects_client       = get_post_meta( get_the_ID(), $prefix . 'projects_client', true );
$projects_location     = get_post_meta( get_the_ID(), $prefix . 'projects_location', true );
$projects_surface_area = get_post_meta( get_the_ID(), $prefix . 'projects_surface_area', true );
$projects_gallery      = get_post_meta( get_the_ID(), $prefix . 'projects_gallery', true );
$data_section_id       = uniqid();
wp_enqueue_style( 'camera-flexslider-css', get_template_directory_uri() . '/libs/flexslider/flexslider.css', array() );
wp_enqueue_script( 'camera-flexslide-script', get_template_directory_uri() . '/libs/flexslider/jquery.flexslider-min.js', false, true );
?>
	<div class="container">
		<?php breadcrumb_trail(); ?>
		<div class="single-projects-container">
			<div class="row top-projects">
				<div class="col-md-9">
					<div class="projects-slideshow" id="post_slideshow_<?php echo esc_attr( $data_section_id ) ?>">
						<div id="projects-slider" class="projects-single-slider">
							<ul class="slides">
								<?php
								$index = 0;

								foreach ( $projects_gallery as $image ) {
									$img    = '';
									$resize = matthewruddy_image_resize( $image, 870, 534 );
									if ( $resize != null ) {
										$img = $resize['url'];
									}
									?>
									<li>
										<a href="<?php echo esc_url( $image ) ?>" data-index="<?php echo esc_attr( $image ) ?>" data-rel="prettyPhoto[projects-gallery]">
											<img alt="projects" src="<?php echo esc_url( $img ) ?>" />
										</a>
									</li>
								<?php } ?>
							</ul>
						</div>
						<div id="projects-carousel" class="projects-single-slider">
							<ul class="slides">
								<?php
								foreach ( $projects_gallery as $image ) {
									$img    = '';
									$resize = matthewruddy_image_resize( $image, 170, 133 );
									if ( $resize != null ) {
										$img = $resize['url'];
									}
									?>
									<li class="projects-thumbnail">
										<a href="<?php echo esc_url( $img ) ?>" data-index="<?php echo esc_attr( $image ) ?>">
											<img alt="projects" src="<?php echo esc_url( $img ) ?>" />
										</a>
									</li>
								<?php } ?>
							</ul>
						</div>
					</div>

				</div>
				<div class="col-md-3 projects-attribute">
					<h3 class="cortana-title">
						<?php echo __( 'Thông tin dự án:', 'cortana' ) ?>
					</h3>
					<div class="projects-info border-primary-color">
						<?php if( $projects_desc ) : ?>
							<div class="projects-short-descripton">
								<p><?php echo esc_html( $projects_desc ) ?></p>
							</div>
						<?php endif; ?>
						<?php if( $projects_status ) : ?>
							<div class="projects-info-box">
								<span class="heading-font"><?php echo __( 'Tình trạng', 'cortana' ) ?></span>

								<div class="projects-term bold-color"><?php echo esc_html( $projects_status ) ?></div>
							</div>
						<?php endif; ?>
						<?php if( $projects_client ) : ?>
							<div class="projects-info-box">
								<span class="heading-font"><?php echo __( 'Khách hàng', 'cortana' ) ?></span>

								<div class="projects-term bold-color"><?php echo esc_html( $projects_client ) ?></div>
							</div>
						<?php endif; ?>
						<?php if( $projects_location ) : ?>
							<div class="projects-info-box">
								<span class="heading-font"><?php echo __( 'Địa chỉ', 'cortana' ) ?></span>

								<div class="projects-term bold-color"><?php echo esc_html( $projects_location ) ?></div>
							</div>
						<?php endif; ?>
						<?php if( $projects_surface_area ) : ?>
							<div class="projects-info-box">
								<span class="heading-font"><?php echo __( 'Diện tích', 'cortana' ) ?></span>

								<div class="projects-term bold-color"><?php echo esc_html( $projects_surface_area ) ?></div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="projects-content">
				<?php the_content() ?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		(function ($) {
			"use strict";
			$(document).ready(function () {
				$('#projects-carousel').flexslider({
					animation    : "slide",
					controlNav   : false,
					animationLoop: false,
					slideshow    : false,
					itemWidth    : 170,
					itemMargin   : 15,
					rtl          : true,
					asNavFor     : '#projects-slider'
				});
				$('#projects-slider').flexslider({
					animation    : "slide",
					directionNav : true,
					controlNav   : false,
					animationLoop: false,
					slideshow    : true,
					rtl          : true,
					sync         : "#projects-carousel",
					start        : function (slider) {
						slider.removeClass('loading');
					}
				});
			});
		})(jQuery);
	</script>

<?php get_footer(); ?>