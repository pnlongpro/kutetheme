<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 6/18/2016
 * Time: 3:37 AM
 */
//Template Name: Projects

get_header();


// Start the loop.
while ( have_posts() ) : the_post();
	$id = get_the_ID();
endwhile;

// Default option
$kt_sidebar_are = kt_option( 'kt_sidebar_are', 'left' );

// Page option
$kt_page_layout          = kt_get_post_meta( $id, 'kt_page_layout', '' );
$kt_show_page_breadcrumb = kt_get_post_meta( $id, 'kt_show_page_breadcrumb', 'show' );

if ( $kt_page_layout != "" ) {
	$kt_sidebar_are = $kt_page_layout;
}

$sidebar_are_layout = 'sidebar-' . $kt_sidebar_are;

if ( $kt_sidebar_are == "left" || $kt_sidebar_are == "right" ) {
	$col_class = "main-content col-xs-12 col-sm-8 col-md-9";
} else {
	$col_class = "main-content page-full-width col-sm-12";
}
?>
	<div id="primary" class="content-area <?php echo esc_attr( $sidebar_are_layout ); ?>">
		<main id="main" class="site-main">
			<?php
			if ( $kt_show_page_breadcrumb == 'show' ) {
				breadcrumb_trail();
			}
			?>
			<div class="row">
				<div class="main-content page-full-width col-sm-12">
					<div class="projects-wrapper">
						<div class="row">
					<?php
					// Start the loop
					//.
					$atts = array();
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args  = array(
						'post_type'           => 'projects',
						'post_status'         => 'publish',
						'ignore_sticky_posts' => 1,
						'posts_per_page'      => 4,
						'suppress_filter'     => true,
						'paged'=>$paged
					);
					query_posts($args);
					while ( have_posts() ) : the_post();
						?>
						<div class=" col-md-3 col-sm-6 col-xs-12">
							<div class="project-items">
							<?php if ( has_post_thumbnail() ): ?>
								<div class="post-thumb image-hover2">
									<a href="<?php the_permalink() ?>">
										<?php
										$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "full" );
										$resize = matthewruddy_image_resize( $thumbnail_src[0], 270, 391 );
										if ( $resize != null ) {
											$img = $resize['url'];
										}
										if ( $thumbnail_src ):?>
											<img alt="<?php the_title() ?>"
												 class="owl-lazy attachment-post-thumbnail wp-post-image" src="<?php echo esc_url( $img ); ?>" data-src="<?php echo esc_url( $thumbnail_src[0] ) ?>" />
										<?php else: ?>
											<img width="<?php echo esc_attr( $thumbnail_src[1] ) ?>" height="<?php echo esc_attr( $thumbnail_src[2] ) ?>" alt="<?php the_title() ?>"
												 class="owl-lazy attachment-post-thumbnail wp-post-image" src="<?php echo esc_url( $temping_post_thumbnail ) ?>" />
										<?php endif; ?>
									</a>

								</div>
								<h3 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
							<?php endif; ?>
							</div>
						</div>
						<?php
						// End the loop.
					endwhile;
					?>

						</div>
						<div class="blog-paging clearfix">
							<?php kt_paging_nav();?>
						</div>
					</div>
				</div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>