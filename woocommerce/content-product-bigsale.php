<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 6/20/2016
 * Time: 1:55 AM
 */
?>
<div class="deal regular ">
	<div class="deal-discount triangle">
		<div class="deal-discount-text">Giảm</div>
		<div class="deal-discount-number">45%</div>
	</div>
	<!-- /.deal-discount -->
	<div class="row">
		<div class="col-xs-12">
			<div class="deal-img ">
				<a href="http://tiki.vn/vu-tru-trong-vo-hat-de.html?ref=c316.c3130.c3259.c3460.c5092.c5270.c5374.c2766.c3133.c3326.c3327.c3452.c4291.c5094.c5103.c5297.c5376.c5571.c6111.c2288.c2494.c3141.c3328.c3535.c4981.c5177.c5236.c5807.c5980.c3503.c5572." class="ga-hot-deal" data-name="KTTĐ - Vũ Trụ Trong Vỏ Hạt Dẻ" data-url="http://tiki.vn/vu-tru-trong-vo-hat-de.html?ref=c316.c3130.c3259.c3460.c5092.c5270.c5374.c2766.c3133.c3326.c3327.c3452.c4291.c5094.c5103.c5297.c5376.c5571.c6111.c2288.c2494.c3141.c3328.c3535.c4981.c5177.c5236.c5807.c5980.c3503.c5572." data-area="gio-vang-gia-soc" target="_blank">
					<img data-src="https://vcdn.tikicdn.com/cache/200x200/media/catalog/product/v/u/vutrutrongvohatde.jpg" alt="KTTĐ - Vũ Trụ Trong Vỏ Hạt Dẻ" class="img-responsive lazy" src="https://vcdn.tikicdn.com/cache/200x200/media/catalog/product/v/u/vutrutrongvohatde.jpg" style="display: inline;" kasperskylab_antibanner="on">
				</a>
				<a data-name="KTTĐ - Vũ Trụ Trong Vỏ Hạt Dẻ" data-image="https://vcdn.tikicdn.com/cache/200x200/media/catalog/product/v/u/vutrutrongvohatde.jpg" data-url="http://tiki.vn/vu-tru-trong-vo-hat-de.html?ref=c316.c3130.c3259.c3460.c5092.c5270.c5374.c2766.c3133.c3326.c3327.c3452.c4291.c5094.c5103.c5297.c5376.c5571.c6111.c2288.c2494.c3141.c3328.c3535.c4981.c5177.c5236.c5807.c5980.c3503.c5572." data-description="Trong quyển “Vũ trụ trong vỏ hạt dẻ”, Stephen Hawking đưa chúng ta đến khía cạnh nổi trội của vật lý lý thuyết, ở đó sự thất thường còn lạ lùng hơn hư cấu, để giảng giải - bằng ngôn ngữ bình thường (không phải ngôn ngữ của người chuyên môn) - những nguyên tắc điều khiển vũ trụ của chúng ta.
Cũng giống như số đông trong cộng đồng những nhà vật lý lý thuyết. Stephen Hawking đang tìm kiếm để khám phá cái cốt lõi của khoa học – Thuyết vạn vật nằm trong tâm củ vũ trụ.
Bằng cách viết dí dỏm và dễ tiếp cận, ông dẫn dắt chúng ta vào hành trình khám phá những bí mật của vũ trụ - từ siêu trọng lượng đến siêu đối xứng, từ thuyết lượng tử đến thuyết M, từ phép chụp ảnh giao thoa laser đến tính hai mặt (tính đối ngẫu).
Ông đưa chúng ta đến biên giới hoang dã của khoa học, nơi mà lý thuyết siêu dây và p mạng có thể nắm giữ mang mối cho điều bí ẩn. Và ông để chúng ta lại đằng sau hậu trường của một trong những cuộc phiêu lưu trí tuệ hấp dẫn nhất của ông khi ông tìm cách kết nối Thuyết tương đối tổng quát của Einstein với ý tưởng về những lịch sử đa dạng của Feynman vào trong một thuyết thống nhất hoàn chỉnh, một thuyết sẽ giải thích mọi thứ xảy ra trong vũ trụ.
Đây là quyển sách cần thiết cho tất cả chúng ta, những người muốn tìm hiểu vũ trụ chúng ta đang sống.  “Cách viết gãy gọn về bản chất của khoa học và vũ trụ… Giáo sư Hawking quả là một thiên tài.
” – The New York Times  “Độc giả sẽ có cảm giác thích thú khi đọc quyển sách cũng giống như sự say mê của Hawking khi viết cuốn sách nà.” – The Dallas Morning News  “Niềm đam mê vũ trụ của Hawking khiến ông trở thành tác giả mà độc giả phải tìm đọc.” – Los Angeles Times  “Nội dung rất dễ hiểu và hình ảnh đầy thông tin…một giới thiệu xuất sắc về một số ý tưởng lớn nhất trong vật lý ” – Science News  “Một cuốn sách thân thiện với độc giả, cả cách viết và hình minh học” – USA Today" target="_blank" class="deal-social share-facebook-link">
					<i class="fa fa-facebook"></i>
				</a>
				<!-- /.deal-social -->
				<span class="deal-gift js-product-gift-icon" data-original-title="- Mua đơn hàng sách bất kỳ được giảm giá 20% khi mua Rexona cho đơn hàng tiếp sau<br />- Mua bất kỳ ĐH trên Tiki.vn ( trừ hàng điện máy) tặng Combo Mẫu thử &nbsp;dầu gội trị gàu 5ml+ dầu xả 5ml Selsun<br />- Tặng sách tự chọn cho đơn hàng sách từ 400k<br />- Tặng sách tô màu Harry Potter cho ĐH Sách và chì màu từ 300k<br />"></span>
			</div>
			<!-- /.deal-img -->
		</div>
		<div class="col-xs-12">
			<div class="deal-info">
				<div class="deal-info-panel">
					<div class="deal-name">
						<a href="http://tiki.vn/vu-tru-trong-vo-hat-de.html?ref=c316.c3130.c3259.c3460.c5092.c5270.c5374.c2766.c3133.c3326.c3327.c3452.c4291.c5094.c5103.c5297.c5376.c5571.c6111.c2288.c2494.c3141.c3328.c3535.c4981.c5177.c5236.c5807.c5980.c3503.c5572." class="ga-hot-deal" data-name="KTTĐ - Vũ Trụ Trong Vỏ Hạt Dẻ" data-url="http://tiki.vn/vu-tru-trong-vo-hat-de.html?ref=c316.c3130.c3259.c3460.c5092.c5270.c5374.c2766.c3133.c3326.c3327.c3452.c4291.c5094.c5103.c5297.c5376.c5571.c6111.c2288.c2494.c3141.c3328.c3535.c4981.c5177.c5236.c5807.c5980.c3503.c5572." data-area="gio-vang-gia-soc" target="_blank" title="KTTĐ - Vũ Trụ Trong Vỏ Hạt Dẻ">KTTĐ - Vũ Trụ Trong Vỏ Hạt Dẻ</a>
					</div>
					<!-- /.deal-name -->
					<div class="deal-price">
						<div>
							<strike>75.000&nbsp;₫</strike>
						</div>
						<div class="deal-price-sale">41.000&nbsp;₫</div>
					</div>
					<!-- /.deal-price -->
				</div>
				<!-- /.deal-info-panel.left -->
				<div class="deal-remain">Còn <strong>200</strong> sản phẩm</div>
				<div class="deal-info-panel">
					<div class="deal-btn">

						<button type="button" class="buy-now-btn remind btn-addtocart js-product-gift-icon" data-id="20359" data-email="timiwon@gmail.com" data-original-title="<i>Gửi email hay thông báo khi mở bán sản phẩm</i>" data-area="gio-vang-gia-soc">
							Nhắc tôi
						</button>
					</div>
					<!-- /.deal-btn -->
				</div>
				<!-- /.deal-info-panel.right -->
			</div>
			<!-- /.deal-info -->
		</div>
	</div>
</div>
