<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 6/21/2016
 * Time: 9:06 AM
 */
?>
<div class="product-deal-tab">
	<?php
		global $product;
	$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "full" );
	if( $product->regular_price != '' ) {
		$percentage    = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
	}else{
		$percentage = '0';
	}

	$img = '';
	$resize    = matthewruddy_image_resize( $thumbnail_src[0], 200, 200 );
	if ( $resize != null ) {
		$img = $resize['url'];
	}
	?>
	<div class="entry-thumnail">
		<a href="<?php the_permalink() ?>">
			<img src="<?php echo $img ?>" alt="<?php echo get_the_title( $post->ID ) ?>">
		</a>
		<div class="deal-discount">
			<?php
			echo '<div class="deal-discount-text">Giảm</div>';
			echo '<div class="deal-discount-number">' . $percentage . '%</div>';
			?>
		</div>
	</div>
	<div class="entry-content">
		<div class="deal-info-panel">
			<div class="entry-title">
				<h3 class="product-name">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

			</div>
			<div class="deal-price">
				<?php
				/**
				 * woocommerce_after_shop_loop_item_title hook
				 * @hooked woocommerce_template_loop_price - 5
				 * @hooked woocommerce_template_loop_rating - 10
				 */
				do_action( 'kt_after_shop_loop_item_title' );
				?>
			</div>
		</div>
		<div class="deal-info-panel">
			<?php
			woocommerce_template_loop_add_to_cart();
			?>
		</div>
	</div>
</div>
